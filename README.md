



Inside the nodes you have:
- Namespaces (like a vpc)
- Services (endpoints for pods, like LB)
- Pods (container + launch templates)





### Prerequisities and Pre install notes

There are different ways to get a k8 cluster. It's generally a bit tricky to set up by yourself. The following options are:

- use kuberspray (ansible)
- kubeadm

Other services of kubernetes that are managed:

- AWS EKS
- Google GKE


Different distro of K8:

- k8 official
- minikube
- MicroK8s

### Our setup

We'll use Kubespray:



We're going to need 
- 1 k8 control panel
- 2 k8 worker nodes
- 1 ansible machine with the right access

Minimum requirements:

- 16gm ram
- 4 cpu

but also smaller, we'll use a t4g.medium 

# Setup Steps

### 1) Start Machines
We need 4 machines (t3a.xlarge)
Start these and give them appropriate tags:

- Ansible 
- k8 control panel
- k8 agent A
- k8 agent B

### 2) Allow SG networking

Ansible will have to connect to k8 machines to setup.

Allow ansible into these mamchines- we can just add a group that allow cohort9_home_ips.

### 3) Create and Share Key Pairs

In the ansible machine:

- create a new key pair 
```bash
$ ssh-keygen 
> add a home/user/nicoleghessen/
> no password 
```

Login to our k8 machines:

- Add/append the public key pair to the authorized_keys file

**Test** step 3 and 4 by trying to login to each k8 machine via the ansible machine using the new keypair and the **PRIVATE IP**

Note- only the private IP will allow you access in.
If you don't manually login the first time, ansible will require the option of StrictlyHostKeyChecking to be off.

### 4) Clone Kubespray to ansible machine 

**note** we might need these dependencies

```bash
# python 3 which already comes with pip
# git 

```

Git clone this repo


```bash
$ git clone https://github.com/kubernetes-sigs/kubespray.git

```

Install dependencies
```bash

sudo apt update 
sudo apt install python3-pip -y
```

Run the requirements.txt
```bash
# Install dependencies from ``requirements.txt``
pip3 install -r requirements.txt
```




### 5) Edit Inventory File in Kubespray & Other Setup

Kubespray has a sample inventory file we can copy and edit.

```bash

# Copy ``inventory/sample`` as ``inventory/mycluster``
cp -rfp inventory/sample inventory/mycluster

```

On the inventory file add your k8s private IPs and specify the ssh key. SSH can also be specified when calling the ansible playbook. 

In the `kubespray/inventory/mycluster/inventory.ini` uncomment the 3 first nodes and add the private IP to the `ansible _host` and to the `IP`:


```YAML
## copy of your inventory yaml from my cluster here
node1 ansible_host=172.31.23.172   ip=172.31.23.172 etcd_member_name=etcd1
node2 ansible_host=172.31.20.205   ip=172.31.20.205 etcd_member_name=etcd2
node3 ansible_host=172.31.23.163   ip=172.31.23.163 etcd_member_name=etcd3
```
Then uncomment the correct sections for node1 to be the control panel and node2 and node3 for the kube_nodes. Also uncomment the etcd which is k8's monitoring:

```YAML
[kube_control_plane]
node1
# node2
# node3

[etcd]
node1
node2
node3

```
Also specift the key for each node:
```YAML
node1 ansible_host=172.31.23.172   ip=172.31.23.172 etcd_member_name=etcd1 ansible_ssh_private_key_file=/home/ubuntu/.ssh/k8_pair ansible_user=ubuntu
node2 ansible_host=172.31.20.205   ip=172.31.20.205 etcd_member_name=etcd2 ansible_ssh_private_key_file=/home/ubuntu/.ssh/k8_pair ansible_user=ubuntu
node3 ansible_host=172.31.23.163   ip=172.31.23.163 etcd_member_name=etcd3 ansible_ssh_private_key_file=/home/ubuntu/.ssh/k8_pair ansible_user=ubuntu

```


```bash

# Return to just kubespray path then run:
# Update Ansible inventory file with inventory builder
declare -a IPS=(172.31.23.172  172.31.20.205 172.31.23.163)
CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}

# Review and change parameters under ``inventory/mycluster/group_vars``
cat inventory/mycluster/group_vars/all/all.yml
cat inventory/mycluster/group_vars/k8s_cluster/k8s-cluster.yml

```


### 6) Run Playbook 

```bash
$ sudo apt install ansible 

$ ansible-playbook -i ~/kubespray/inventory/mycluster/inventory.ini --become-user=root ~/kubespray/cluster.yml -b 
```