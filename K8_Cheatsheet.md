## Namespaces
Namespaces provides a mechanism for isolating groups of resources within a single cluster. Names of resources need to be unique within a namespace, but not across namespaces.

```bash
$ kubectl get namespace
$ kubectl create namespace <name>
$ kubectl delete namespaces <name>
```

## Deployment Files 

A Deployment provides declarative updates for Pods and ReplicaSets.

You describe a desired state in a Deployment, and the Deployment Controller changes the actual state to the desired state at a controlled rate. You can define Deployments to create new ReplicaSets, or to remove existing Deployments and adopt all their resources with new Deployments.

<u>The langauge used is:</u> **YAML**

<u>What makes it a deployment file?</u>
- template that creates what you define 

<u>The 3 sections of a deployment file?</u>
- apiVersion (apps/v1)
- Kind (Deployment)
- The metadata with a spec section to define the replicas and configurations related to the deployment

<u>Why are labels important?</u>
A common set of labels allows tools to work interoperably, describing objects in a common manner that all tools can understand and recommended labels describe applications in a way that can be queried.

<u>Where to specify the container image?</u>
In the file you specify the container and image after you state the apiVersion, Kind and Spec.

<u>How do you run the deployment file into your new Namespace?</u>
```bash
$ kubectl apply -f nginx-deployment.yaml --namespace nginx
```

<u>How do your check how many pods are running?</u>
```bash
$ kubectl get pods --all-namespaces
# example for the tutorial we used: 
#kubectl get pods -n nginx
```

<u>How to kill a pod?</u>
```bash
$ kubectl delete pod <pod_name>
# example for the nginx tutroial:
# $ kubectl delete pod nginx-deployment-9456bbbf9-99jbl -n nginx
```

<u>How do you describe a pod?</u>
```bash
# nginx example used
$ kubectl describe pod nginx-deployment-9456bbbf9-m88p6 -n nginx
```

<u>How to ssh into a k8 pod?</u>
```bash
#syntax
$ kubectl exec --stdin --tty <pod_name> -n <namespace_name> -- /bin/bash
# nginx example:
$ kubectl exec --stdin --tty nginx-deployment-9456bbbf9-m88p6 -n nginx -- /bin/bash
```
**Please Note** The double dash (--) separates the arguments you want to pass to the command from the kubectl arguments.

<u>How do you modify a deployment setup?</u>
Change the three sections of the deployment file and define what you want to change.

<u>Change the Image?</u>
You can configure your pod with a grace period (for example 30 seconds or more, depending on container startup time and image size) and set "imagePullPolicy: "Always". 
Use:
```bash
kubectl delete pod pod_name
```
A new container will be created and the latest image automatically downloaded, then the old container terminated.

```bash 
spec:
  terminationGracePeriodSeconds: 30
  containers:
  - name: my_container
    image: my_image:latest
    imagePullPolicy: "Always"
```

<u>Change the number of pods?</u>
Change the replicas number 

**Then re-run the apply command to make it work**
```bash
$ kubectl apply -f nginx-deployment.yaml --namespace nginx
```


### Other command to remember- used when reading K8s tutorial
```bash
$ nano nginx-deployment.yaml then pasted in the config stuff they listed.
```

James: kubectl scale deployment/nginx-deployment -n james --replicas=3
kubectl set image deployment/nginx-deployment nginx=nginx:1.16.1



## Service Files





